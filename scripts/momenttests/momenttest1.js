
print(moment().format('MMMM Do YYYY, h:mm:ss a'));

print(moment().toArray());

print(moment(new Date('2012-03-03')).isAfter(new Date('3 May 2020')));


// query capabilities of moment.js
print('------------------------------------');
print(moment('22 June 2019 19:45:00').toArray());

var d1 = moment().subtract(7, 'days').add(1.5, 'months').subtract(10, 'minutes').add(4.3, 'hours');
var d2 = moment();
var d3 = moment().subtract(2, 'hours');

print(d1.diff(d2, 'hours'));
print(d2.diff(d3, 'hours'));

var m = moment('2013-03-01', 'YYYY-MM-DD');
print();

// D = day, T = time
// {D,T} comp {D,T} => {D,T}
// {D,T} comp {T} => {T}
// {D,T} comp {D} => {D}
// {D} comp {T} => {D} --> compare time with '00:00'

