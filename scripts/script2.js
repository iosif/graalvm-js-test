// arrays - Nashorn style

var IntArr = Java.type("int[]");

var iarr = new IntArr(5);

for (var i = 0; i < 5; i++) {
    iarr[i] = (i + 1) * 10;
}

print(" -- [ ARRAY ] --------------------- ");
for (var i = 0; i < 5; i++) {
    print("Elem: " + iarr[i]);
}
//
// // arrays - Rhino style
// var JArray = Java.type('java.lang.reflect.Array');
// var JString = Java.type('java.lang.String');
// var sarr = JArray.newInstance(JString, 5);
//
// for (var i = 0; i < 5; i++) {
//     var value = (i + 1) * 10 + 7;
//     // sarr[i] =
//     Array.set(sarr, i, value);
// }
//
// print(" -- [ [s] ARRAY ] --------------------- ");
// for (var i = 0; i < 5; i++) {
//     print("[s] Elem: " + sarr[i]);
// }

// print informations about GraalVM
print(Graal.versionJS);
print(Graal.versionGraalVM);

obj.name = 'Jack';
print(obj.name);
print(obj.name2);
print(obj.name3);