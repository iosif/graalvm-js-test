
var String = Java.type('java.lang.String');

print('[Script] Immediately after opening the script file.');
var File = Java.type('java.io.File');
var FileInputStream = Java.type('java.io.FileInputStream');
var Byte = Java.type('java.lang.Byte');


var LocalDateTime = Java.type('java.time.LocalDateTime');

var currentFile = new File('script.js');

print('Can read? ' + currentFile.canRead());
print('Can write? ' + currentFile.canWrite());
print('Can execute? ' + currentFile.canExecute());

print(new Date());
print(LocalDateTime.now());

var s1 = new String('Hello World');
var s2 = s1.concat(new String('!!!'));
print(s2);

// -------------------------------------------------- //
// var file = new File("scripts/script.js");
// var fis = new FileInputStream(file);
// var data = [...new Array(file.length())];
// fis.read(data);
// fis.close();
//
// // var str = new String(data, "UTF-8");
// print(file.getAbsolutePath());