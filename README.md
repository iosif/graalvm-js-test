# Replacing Nashorn (JavaScript Engine)
This is a research project with the purpose of finding a fit replacement for Nashorn JavaScript Engine.
This project tests the `GraalVM` polyglot engine as a replacement for JavaScript's engine *Nashorn*.

## Main demo
The most important and most complete file that tests the GraalVM project is the `Application` file 
located in the package `org.graalvm.main`.

## Dependecies
This is a **maven** project. Inside the file `pom.xml` are the main dependencies. Below is a copy of those dependencies:
```xml
<dependency>
    <groupId>org.graalvm.sdk</groupId>
    <artifactId>graal-sdk</artifactId>
    <version>19.0.0</version>
</dependency>
<dependency>
    <groupId>org.graalvm.js</groupId>
    <artifactId>js</artifactId>
    <version>19.0.0</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.graalvm.js</groupId>
    <artifactId>js-scriptengine</artifactId>
    <version>19.0.0</version>
</dependency>
```

## Tests
The folder `scripts` contains some JavaScript script files that are referenced from Java code inside `src` folder.

## Versions
* Needs *OpenJDK 11* because some parts of **GraalVM** are shipped with this version of the *JDK*.

---

### References
* [GraalVM](https://www.graalvm.org)
* [GraalVM's embedding reference](https://www.graalvm.org/docs/reference-manual/embed/)
* [GraalVM basic tutorial](https://amarszalek.net/blog/2018/06/08/evaluating-javascript-in-java-graalvm/)
* [Another GraalVM article](https://chrisseaton.com/truffleruby/tenthings/)
* [Introduction to Nashorn](https://www.baeldung.com/java-nashorn)