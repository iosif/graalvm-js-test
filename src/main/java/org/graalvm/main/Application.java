package org.graalvm.main;


import org.graalvm.main.proxy.ComputedArray;
import org.graalvm.main.proxy.ComputedExecutable;
import org.graalvm.main.proxy.ComputedObject;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Application {
//    private static Value evalJS(String sourceCode) {
//        try(Context context = Context.create("js")) {
//            Value parse = context.eval("js", "JSON.parse");
//            Value stringify = context.eval("js", "JSON.stringify");
//            Value result = stringify.execute(parse.execute(sourceCode), null, 2);
//            return result;
//        }
//    }

    private static void computeExecutable(Context context) {
        ComputedExecutable computedExecutable = new ComputedExecutable();
        context.getBindings("js").putMember("fnSum", computedExecutable);

        System.out.println("Result for sum is: " + context.eval("js", "fnSum(1.1, 2.2, 3.3, 4.4)").asDouble());
    }

    private static void computedObject(Context context) {
        ComputedObject computedObject = new ComputedObject();
        context.getBindings("js").putMember("obj", computedObject);

        String strValue =
            "obj.name = 'Jack';\n" +
            "print(obj.name);\n" +
            "print(obj.name2);\n" +
            "print(obj.name3);";

        Value result = context.eval("js", strValue);
    }

    private static void computedArrays(Context context) {
        ComputedArray computedArray = new ComputedArray();
        context.getBindings("js").putMember("arr", computedArray);
        long result = context.eval("js", "arr[1] + arr[1000000000]").asLong();
        assert result == 2000000002L;
    }

    private static void lookupJavaTypesFromGuestLanguages(Context context) {
        String repr = "var BigDecimal = Java.type('java.math.BigDecimal'); BigDecimal.valueOf(10).pow(20)";

        java.math.BigDecimal v = context.eval("js", repr).asHostObject();

        assert v.toString().equals("100000000000000000000");
        System.out.println(v.toString().equals("100000000000000000000"));
    }

    private static void accessJavaFromGuestLanguage(Context context) {
        context.getBindings("js").putMember("javaObj", new MyClass());

        String evaluationString = "javaObj.id === 42 && javaObj.text === '42' && javaObj.arr[1] === 42 && javaObj.ret42() === 42";

        boolean valid = context.eval("js", evaluationString).asBoolean();

        assert valid == true;
    }

    private static void accessGuestLanguageDirectly(Context context) {
        Value result = context.eval("js", "({ id: 42, text: '42', arr: [1, 42, 3]})");

        assert result.hasMembers();

        int id = result.getMember("id").asInt();
        assert id == 42;

        String text = result.getMember("text").asString();
        assert text.equals("42");

        Value array = result.getMember("arr");
        assert array.hasArrayElements();
        assert array.getArraySize() == 3;
        assert array.getArrayElement(1).asInt() == 42;
    }

    private static Context getJSContext() {
        return Context.newBuilder("js").allowAllAccess(true).build();
//        return Context.create("js");
    }

    private static Value evalJS(Context context, String sourceCode) {
        Value value = context.eval("js", sourceCode);
        return value;
    }

    private static Value evalJSFile(Context context, String filename) throws IOException {
        Source source = Source.newBuilder("js", new File(filename)).build();
        Value value = context.eval(source);
        return value;
    }

    private static List<Character> getListOfAllLetters() {
        List<Character> chars = new ArrayList<>();

        int start = 65;
        for (int current = start; current < 65 + 26; current++) {
            chars.add((char)current);
        }
        start = 97;
        for (int current = start; current < 97 + 26; current++) {
            chars.add((char)current);
        }

        return chars;
    }

    private static void setGlobals(Context context) {
        getListOfAllLetters().forEach(letter -> evalJS(context, String.format("var %s = 0;", letter)));
    }

    private static List<Map.Entry<String, Value>> getGlobalEnvironment(Context context) {
        List<Map.Entry<String, Value>> values = new ArrayList<>();
        Value bindings = context.getBindings("js");

        for (String key : bindings.getMemberKeys()) {
            Map.Entry<String, Value> entry = new AbstractMap.SimpleEntry<>(key, bindings.getMember(key));
            values.add(entry);
        }

        return values;
    }

    private static void executeObjectDemo(Context context) {
        String objectLiteral = "({ \"name\": 'Jack',\"age\": 22,  \"city\": 'Paris',  \"country\": 'France',  \"favoriteNumbers\": [44, 345, 892, 899]})";

        Value object = context.eval("js", objectLiteral);
//        Value object = context.getBindings("js").getMember("person");

        // print all key-value pairs
        for (String key : object.getMemberKeys()) {
            System.out.println("Key #" + key + ": Value " + object.getMember(key));
        }
    }

    private static void executeFunctionDemo(Context context) {
        String printArrayJSFunc = "(arr) => {\n" +
                "    for (var i = 0; i < arr.length; i++) {\n" +
                "        print(arr[i]);\n" +
                "    }\n" +
                "}";

        String printArrayJSFunc2 = "(function printArrayJS(arr) {\n" +
                "    for (var i = 0; i < arr.length; i++) {\n" +
                "        print(arr[i]);\n" +
                "    }\n" +
                "})";

        Value printArrayJS = context.eval("js", printArrayJSFunc);

        // execute second function
        Value printArrayJS2 = context.eval("js", printArrayJSFunc2);
//        Value printArrayJS2 = context.eval("js", "printArrayJS");

        System.out.println("Can execute? " + printArrayJS.canExecute());

        if (printArrayJS.canExecute()) {
            // printArrayJS.executeVoid(new int[] { 10, 11, 22, 45, 65, 77, 89, 98 });
            printArrayJS.execute(new int[] { 10, 11, 22, 45, 65, 77, 89, 98 }, null, 2);
        }

        if (printArrayJS2.canExecute()) {
            System.out.println("The second can execute as well!");
            printArrayJS2.execute(new String[] { "ONE", "TWO", "THREE"}, null, 2);
        } else {
            System.out.println("The second can't execute!");
        }
    }

    public static void main(String[] args) throws IOException, ScriptException, NoSuchMethodException, ScriptException {
        Context jsContext = getJSContext();

        executeFunctionDemo(jsContext);
        executeObjectDemo(jsContext);
        accessGuestLanguageDirectly(jsContext);
        accessJavaFromGuestLanguage(jsContext);
        lookupJavaTypesFromGuestLanguages(jsContext);
        computedArrays(jsContext);
        computedObject(jsContext);
        computeExecutable(jsContext);

//
//        String input = "{\"numbers\":[1,2,3, {}, [], 4,5],\"containsNegatives\":false}";
//        evalJS(jsContext,"var x = 10;");
//
//        evalJS(jsContext,"print(x); x++;");
//
//        setGlobals(jsContext);
//        evalJS(jsContext,"print(x); x++;");
//
//        evalJS(jsContext, "var name = 'abcdef';");
//
//        evalJS(jsContext, "var fn = function() { print('fn'); };");
//
//        evalJS(jsContext, "let value = 2 + 3 * 4 + 5 * 6 * 7 + 8 * 9 * 10");
//
//        getGlobalEnvironment(jsContext).forEach(entry -> {
//            System.out.println(entry.getKey() + ": " + entry.getValue());
//        });
//
//        List<Map.Entry<String, Value>> globalEnvironmentList = getGlobalEnvironment(jsContext);
//        Collections.sort(globalEnvironmentList, Comparator.comparing(Map.Entry::getKey));
//
//        evalJS(jsContext, "var arr = [1, 2, 3, 4, 5];");
//
//        globalEnvironmentList.forEach(entry -> System.out.println(entry.getKey() + (entry.getValue().canExecute() ? " is a function." : entry.getValue().toString())));

//        evalJSFile(jsContext, String.join(File.separator, List.of("scripts", "script.js")));

        evalJSFile(jsContext, String.join(File.separator, List.of("scripts", "script2.js")));
    }
}
