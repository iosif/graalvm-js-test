package org.graalvm.main;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MomentJSLibTest {

    private static Context getJSContext() {
        return Context.newBuilder("js").allowAllAccess(true).build();
    }

    private static Value evalJS(Context context, String sourceCode) {
        Value value = context.eval("js", sourceCode);
        return value;
    }

    private static Value evalJSFile(Context context, String filename) throws IOException {
        Source source = Source.newBuilder("js", new File(filename)).build();
        Value value = context.eval(source);
        return value;
    }

    public static void main(String[] args) throws IOException {
        Context jsContext = getJSContext();

        // add global bingings
        evalJSFile(jsContext, String.join(File.separator, List.of("scripts", "lib", "moment.min.js")));

        // run the tests
        evalJSFile(jsContext, String.join(File.separator, List.of("scripts", "momenttests", "momenttest1.js")));

    }
}
