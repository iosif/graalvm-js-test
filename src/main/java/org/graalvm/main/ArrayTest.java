package org.graalvm.main;

import java.lang.reflect.Array;

public class ArrayTest {
    public static void main(String[] args) {
        // create an array using reflection
        Object newArray = Array.newInstance(String.class, 5);

        // set the array
        for (int i = 0; i < 5; i++) {
            Array.set(newArray, i, "Hello #" + i);
        }

        // print the array
        System.out.println("-- ARRAY -------------------------");
        for (int i = 0; i < 5; i++) {
            Object value = Array.get(newArray, i);

            System.out.println(value);
        }
    }
}
