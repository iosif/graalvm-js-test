package org.graalvm.main.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;

import java.util.HashMap;
import java.util.Map;

public class ComputedObject implements ProxyObject {
    private Map<String, Value> elements = new HashMap<>();

    @Override
    public Object getMember(String key) {
        return elements.get(key);
    }

    @Override
    public Object getMemberKeys() {
        return elements.keySet();
    }

    @Override
    public boolean hasMember(String key) {
        return elements.containsKey(key);
    }

    @Override
    public void putMember(String key, Value value) {
        elements.put(key, value);
        elements.put(key + "2", value);
    }
}
