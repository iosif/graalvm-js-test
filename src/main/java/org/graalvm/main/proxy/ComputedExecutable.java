package org.graalvm.main.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyExecutable;

import java.util.Arrays;

public class ComputedExecutable implements ProxyExecutable {

    @Override
    public Object execute(Value... arguments) {
        return Arrays.stream(arguments).mapToDouble(Value::asDouble).sum();
    }
}
