package org.graalvm.main.proxy;

import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyArray;

public class ComputedArray implements ProxyArray {
    @Override
    public Object get(long index) {
        return index * 2;
    }

    @Override
    public void set(long index, Value value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getSize() {
        return Long.MAX_VALUE;
    }
}
