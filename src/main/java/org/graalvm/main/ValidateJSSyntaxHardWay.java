package org.graalvm.main;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ValidateJSSyntaxHardWay {
    private static Context getJSContext() {
        return Context.newBuilder("js").allowAllAccess(true).build();
    }

    private static Value evalJS(Context context, String sourceCode) {
        Value value = context.eval("js", sourceCode);
        return value;
    }

    private static boolean evalJSCheckSyntax(Context context, String sourceCode) {
        try {
            context.eval("js", sourceCode);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static Value evalJSFile(Context context, String filename) throws IOException {
        Source source = Source.newBuilder("js", new File(filename)).build();
        Value value = context.eval(source);
        return value;
    }

    private static boolean evalJSFileCheckSyntax(Context context, String filename) throws IOException {
        Source source = Source.newBuilder("js", new File(filename)).build();
        try {
            context.eval(source);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] args) throws IOException {
        Context jsContext = getJSContext();

        boolean isSyntaxValid = evalJSFileCheckSyntax(jsContext, String.join(File.separator, List.of("scripts", "syntaxtest", "invalid01.js")));

        System.out.println("Valid syntax? " + isSyntaxValid);
        System.out.println("Valid syntax (2nd)? " + evalJSCheckSyntax(jsContext, "false && false && 1 ==== 2 || 0 < 0"));
    }
}
